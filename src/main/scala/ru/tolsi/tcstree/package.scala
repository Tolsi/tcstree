package ru.tolsi

package object tcstree {
  // from http://stackoverflow.com/a/1747172/699934
  class Optionable[T <: AnyRef](value: T) {
    def toOption: Option[T] = if ( value == null ) None else Some(value)
  }

  implicit def anyRefToOptionable[T <: AnyRef](value: T) = new Optionable(value)
}