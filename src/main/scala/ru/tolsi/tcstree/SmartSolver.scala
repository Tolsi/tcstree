package ru.tolsi.tcstree

import scala.util.control.Breaks._

class SmartSolver(val tree: BinaryTree) {

  type State = (BinaryTree, Int)

  case class Result(weight: Int, moves: Int)

  def solve(maxMoves: Int): Result = {
    var prevWeight = tree.leavesWeight
    var newWeight = 0
    var moves = 0
    breakable {
      do {
        val bestNodeToRemove = tree.levelTraverse.par.minBy(n => countWeigthWithChilds(n.toOption))
        tree.removeSubtree(bestNodeToRemove)
        if (moves < maxMoves) {
        newWeight = tree.leavesWeight
          if (newWeight > prevWeight) {
          moves += 1
          prevWeight = newWeight
        } else {
          break()
          }
        } else {
          break()
        }
      } while (tree.levelTraverse.nonEmpty)
    }
    Result(Math.max(prevWeight,newWeight), moves)
  }

  private[tcstree] def countWeigthWithChilds(node: Option[BinaryTreeNode]):Int = {
    node match {
      case Some(n) => n.weight + countWeigthWithChilds(n.leaves._1) + countWeigthWithChilds(n.leaves._2)
      case _ => 0
    }
  }
}
