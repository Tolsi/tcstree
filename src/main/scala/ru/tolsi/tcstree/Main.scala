package ru.tolsi.tcstree

import scala.util.Random

object Main extends App {
  def random(from: Int, to: Int): Int = {
    from + Random.nextInt(to - from)
  }
  
  def fillTreeWithRandomData(binaryTree:BinaryTree, n: Int) = {
    (0 to n).foreach(_ => {
      val nodeWeight = random(-1000, 1000)
      binaryTree.addNode(new BinaryTreeNode(nodeWeight))
    })
  }

  val removeOperations = random(1, 100)
  val nodesCount = random(100, 1000)
  
  val binaryTree = new BinaryTree()
  
  fillTreeWithRandomData(binaryTree, nodesCount)
  
  println(s"Generated leaves weight: ${binaryTree.leavesWeight}")

  val result = new SmartSolver(binaryTree).solve(removeOperations)
  println(s"Result leaves weight: ${result.weight}")
  println(s"Moves done: ${result.moves}")
}

