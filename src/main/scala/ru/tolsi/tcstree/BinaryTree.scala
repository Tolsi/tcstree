package ru.tolsi.tcstree

import scala.collection.GenTraversable

case class BinaryTreeNode(weight: Int, var leaves: (Option[BinaryTreeNode], Option[BinaryTreeNode]) = (None, None), var index: Option[Int] = None)

case class BinaryTree(private var root: Option[BinaryTreeNode] = None) {

  def levelTraverse: GenTraversable[BinaryTreeNode] = this.synchronized {
    root.map(levelTraverse(_).par).getOrElse(Stream.empty)
  }

  def leavesWeight: Int = this.synchronized {
    levelTraverse.foldLeft(0)((w, n) => w + n.weight)
  }

  private def levelTraverse(leaf: BinaryTreeNode): Stream[BinaryTreeNode] = {
    Stream.cons[BinaryTreeNode](leaf,
      leaf.leaves match {
        case (Some(l1), Some(l2)) => levelTraverse(l1) ++ levelTraverse(l2)
        case (Some(l), None) => levelTraverse(l)
        case (None, Some(l)) => levelTraverse(l)
        case _ => Stream.empty
      })
  }

  private var prevIndex: Int = 0

  def weight: Int = this.synchronized {
    levelTraverse.size
  }

  // sequence add only
  def addNode(node: BinaryTreeNode) = this.synchronized {
    node.index = Some(prevIndex + 1)
    prevIndex += 1
    if (root.isEmpty) {
      root = node.toOption
    } else {
      levelTraverse.find(n => n.leaves._1.isEmpty || n.leaves._2.isEmpty).map(n => {
        if (n.leaves._1.isEmpty) {
          n.leaves = (node.toOption, n.leaves._2)
        } else if (n.leaves._2.isEmpty) {
          n.leaves = (n.leaves._1, node.toOption)
        }
      })
    }
  }

  def removeSubtree(nodeToRemove: BinaryTreeNode): Unit = this.synchronized {
    if (root.map(_.index).contains(nodeToRemove.index)) root = None else removeSubtree(root, nodeToRemove)
  }

  private def removeSubtree(currentNodeOpt: Option[BinaryTreeNode], nodeToRemove: BinaryTreeNode): Unit = {
    currentNodeOpt.foreach(currentNode => currentNode.leaves match {
      case (Some(l1), Some(l2)) => if (l1.index == nodeToRemove.index) {
        currentNode.leaves = (None, Some(l2))
      } else if (l2.index == nodeToRemove.index) {
        currentNode.leaves = (Some(l1), None)
      } else {
        removeSubtree(l1.toOption, nodeToRemove)
        removeSubtree(l2.toOption, nodeToRemove)
      }
      case (Some(l), None) => if (l.index == nodeToRemove.index) {
        currentNode.leaves = (None, None)
      } else {
        removeSubtree(l.toOption, nodeToRemove)
      }
      case (None, Some(l)) => if (l.index == nodeToRemove.index) {
        currentNode.leaves = (None, None)
      } else {
        removeSubtree(l.toOption, nodeToRemove)
      }
      case _ =>
    })
  }
}