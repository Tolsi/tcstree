package ru.tolsi.tcstree

import org.scalatest.{FlatSpec, Matchers}

class SmartSolverSpec extends FlatSpec with Matchers {
   it should "solve 1" in {
     val rootNode = BinaryTreeNode(index = Some(1), weight = 4, leaves = (BinaryTreeNode(index = Some(2), weight = 2).toOption, BinaryTreeNode(index = Some(3), weight = 1).toOption))
     val simpleTree = BinaryTree(rootNode.toOption)
     val solver = new SmartSolver(simpleTree)
     val result = solver.solve(1)
     result.weight should be(7)
     result.moves should be(0)
   }
 
   it should "solve 2" in {
     val rootNode = BinaryTreeNode(index = Some(1), weight = 4, leaves = (BinaryTreeNode(index = Some(2), weight = 2, leaves = (BinaryTreeNode(index = Some(4), weight = 2).toOption, BinaryTreeNode(index = Some(5), weight = 1).toOption)).toOption, BinaryTreeNode(index = Some(3), weight = -1).toOption))
     val simpleTree = BinaryTree(rootNode.toOption)
     val solver = new SmartSolver(simpleTree)
     val result = solver.solve(1)
     result.weight should be(9)
     result.moves should be(1)
   }

  it should "solve 3" in {
    val rootNode = BinaryTreeNode(index = Some(1), weight = -4, leaves = (BinaryTreeNode(index = Some(2), weight = -2, leaves = (BinaryTreeNode(index = Some(4), weight = -2).toOption, BinaryTreeNode(index = Some(5), weight = -1).toOption)).toOption, BinaryTreeNode(index = Some(3), weight = -1).toOption))
    val simpleTree = BinaryTree(rootNode.toOption)
    val solver = new SmartSolver(simpleTree)
    val result = solver.solve(50)
    result.weight should be(0)
    result.moves should be(1)
  }

  it should "solve 3.1" in {
    val rootNode = BinaryTreeNode(index = Some(1), weight = 2, leaves =
      (BinaryTreeNode(index = Some(2), weight = -2,
        leaves = (BinaryTreeNode(index = Some(4), weight = -2).toOption,
          BinaryTreeNode(index = Some(5), weight = -1).toOption)).toOption,
        BinaryTreeNode(index = Some(3), weight = -1).toOption))
    val simpleTree = BinaryTree(rootNode.toOption)
    val solver = new SmartSolver(simpleTree)
    val result = solver.solve(1)
    result.weight should be(1)
    result.moves should be(1)
  }

  it should "count weigth with childs" in {
    val rootNode = BinaryTreeNode(index = Some(1), weight = 4, leaves = (BinaryTreeNode(index = Some(2), weight = 2, leaves = (BinaryTreeNode(index = Some(4), weight = 2).toOption, BinaryTreeNode(index = Some(5), weight = 1).toOption)).toOption, BinaryTreeNode(index = Some(3), weight = 1).toOption))
    val simpleTree = BinaryTree(rootNode.toOption)
    val solver = new SmartSolver(simpleTree)
    solver.countWeigthWithChilds(rootNode.toOption) should be(10)
   }
 }
