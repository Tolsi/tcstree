package ru.tolsi.tcstree

import org.scalatest._

class BinaryTreeSpec extends FlatSpec with Matchers {
  it should "make traverse by level" in {
    val thirdNode = BinaryTreeNode(index = Some(3), weight = 1)
    val secondNode = BinaryTreeNode(index = Some(2), weight = 2)
    val rootNode = BinaryTreeNode(index = Some(1), weight = 4, leaves = (secondNode.toOption, thirdNode.toOption))
    val simpleTree = BinaryTree(rootNode.toOption)
    simpleTree.levelTraverse should be(Stream(rootNode, secondNode, thirdNode))
  }

  it should "calculate leaves weight" in {
    val rootNode = BinaryTreeNode(index = Some(1), weight = 4, leaves = (BinaryTreeNode(index = Some(2), weight = 2).toOption, BinaryTreeNode(index = Some(3), weight = 1).toOption))
    val simpleTree = BinaryTree(rootNode.toOption)
    simpleTree.leavesWeight should be(7)
  }

  it should "remove subtree 1" in {
    val thirdNode = BinaryTreeNode(index = Some(3), weight = 1)
    val rootNode = BinaryTreeNode(index = Some(1), weight = 4, leaves = (BinaryTreeNode(index = Some(2), weight = 2).toOption, thirdNode.toOption))
    val simpleTree = BinaryTree(rootNode.toOption)
    simpleTree.removeSubtree(thirdNode)
    rootNode should be(BinaryTreeNode(index = Some(1), weight = 4, leaves = (BinaryTreeNode(index = Some(2), weight = 2).toOption, None)))
  }

  it should "remove subtree 2" in {
    val thirdNode = BinaryTreeNode(index = Some(3), weight = 1)
    val rootNode = BinaryTreeNode(index = Some(1), weight = 4, leaves = (BinaryTreeNode(index = Some(2), weight = 2).toOption, thirdNode.toOption))
    val simpleTree = BinaryTree(rootNode.toOption)
    simpleTree.removeSubtree(rootNode)
    simpleTree should be(BinaryTree())
  }

  it should "remove subtree 3" in {
    val secondNode = BinaryTreeNode(index = Some(2), weight = 2)
    val thirdNode = BinaryTreeNode(index = Some(3), weight = 2)
    val rootNode = BinaryTreeNode(index = Some(1), weight = 4, leaves = (secondNode.toOption, thirdNode.toOption))
    val simpleTree = BinaryTree(rootNode.toOption)
    simpleTree.removeSubtree(secondNode)
    simpleTree should be(BinaryTree(BinaryTreeNode(index = Some(1), weight = 4, leaves = (None, thirdNode.toOption)).toOption))
    simpleTree.removeSubtree(thirdNode)
    simpleTree should be(BinaryTree(BinaryTreeNode(index = Some(1), weight = 4, leaves = (None, None)).toOption))
  }

  it should "calculate weight after the subtree removal" in {
    val rootNode = BinaryTreeNode(index = Some(1), weight = 4, leaves = (BinaryTreeNode(index = Some(2), weight = 2).toOption, BinaryTreeNode(index = Some(3), weight = 1).toOption))
    val simpleTree = BinaryTree(rootNode.toOption)
    simpleTree.removeSubtree(rootNode)
    simpleTree.leavesWeight should be(0)
  }

  it should "make traverse by level after the subtree removal" in {
    val thirdNode = BinaryTreeNode(index = Some(3), weight = 1)
    val secondNode = BinaryTreeNode(index = Some(2), weight = 2)
    val rootNode = BinaryTreeNode(index = Some(1), weight = 4, leaves = (secondNode.toOption, thirdNode.toOption))
    val simpleTree = BinaryTree(rootNode.toOption)
    simpleTree.removeSubtree(rootNode)
    simpleTree.levelTraverse should be(Stream.empty)
  }

  it should "add nodes" in {
    val emptyBinaryTree = BinaryTree()
    emptyBinaryTree.weight should be(0)
    emptyBinaryTree.addNode(BinaryTreeNode(1))
    emptyBinaryTree.weight should be(1)
    emptyBinaryTree.addNode(BinaryTreeNode(2))
    emptyBinaryTree.weight should be(2)
    emptyBinaryTree.addNode(BinaryTreeNode(1))
    emptyBinaryTree.weight should be(3)
  }
}
